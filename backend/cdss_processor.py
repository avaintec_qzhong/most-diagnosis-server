#!/usr/bin/python
# -*- coding: UTF-8 -*-
import pandas as pd
import numpy as np
import tensorflow as tf
from bert4keras.snippets import sequence_padding
from bert4keras.tokenizers import Tokenizer
import json
def data_merge(s_dict):
    def remove_other(data):
        print("remove_data",data)
        if data:
            if "其他" in data:
                data.remove("其他")
            if "不清楚" in data:
                data.remove("不清楚")
            if "未用药" in data:
                data.remove("未用药")
            return data
        else:
            return data

    # "性别，年龄，主述，病症，病史，过敏史，家族史，患病名称"
    complaint = ""
    current_medical_history = ""
    previous_medical_history = ""
    allergy_history = ""
    famliy_history = ""

    #list
    sex = s_dict.get("性别")
    age = s_dict.get("年龄")
    cough_time = s_dict.get("请问咳嗽出现多久了？")
    sputum = s_dict.get("请问咳嗽是否有痰，或痰极少，不易排出？")
    sputum_color_texture = s_dict.get("请问痰的颜色和质地？")
    cough_start_time = s_dict.get("请问咳嗽发作的时间？")
    past_history = s_dict.get("请问患者过去有哪些病史？")
    allergen = s_dict.get("请问患者过敏源？")
    allerge_famliy = s_dict.get("过敏性疾病家族史？")
    choking = s_dict.get("异物呛咳史？")
    respiratory_infection_history = s_dict.get("有呼吸道感染史？")
    symptom = s_dict.get("请问患者有什么伴随症状？")
    medical_history = s_dict.get("请问患者是否用药治疗？")
    medical_ease = s_dict.get("请问患者用药后是否有所缓解？")
    #str
    cough_name = s_dict.get("患病名称","")

    #主述
    if cough_time:
        complaint += "咳嗽{}{}".format(cough_time[0],cough_time[1])
        if symptom:
            if "发热" in symptom:
                complaint += "，伴发热。"

    #病症
    if sputum_color_texture:
        if sputum_color_texture[1] not in ["其他","不清楚"]:
            current_medical_history += sputum_color_texture[1]
        if sputum_color_texture[0] not in ["其他","不清楚"]:
            current_medical_history += sputum_color_texture[0]
        if current_medical_history:
            current_medical_history += ","

    if sputum:
        current_medical_history += "痰量：{}。".format(sputum[0])

    cough_start_time = remove_other(cough_start_time)
    if cough_start_time:
        current_medical_history += "咳嗽时间为："
        for item in cough_start_time:
            current_medical_history += item + "，"
        else:
            current_medical_history = current_medical_history[:-1] + "。"

    symptom = remove_other(symptom)
    if symptom:
        current_medical_history += "伴随症状为："
        # current_medical_history += "伴"
        for item in symptom:
            current_medical_history += item + "，"
        else:
            current_medical_history = current_medical_history[:-1] + "。"

    past_history = remove_other(past_history)
    if past_history:
        for item in past_history:
            previous_medical_history += item + "，"
        else:
            previous_medical_history = previous_medical_history[:-1] + "。"

    allergen = remove_other(allergen)
    if allergen:
        allergy_history += "过敏原为"
        for item in allergen:
            allergy_history += item + "，"
        else:
            allergy_history = allergy_history[:-1] + "。"

    allerge_famliy = remove_other(allerge_famliy)
    if allerge_famliy:
        for item in allerge_famliy:
            famliy_history += item + "，"
        else:
            famliy_history = famliy_history[:-1] + "。"

    choking = remove_other(choking)
    if choking:
        previous_medical_history += "呛咳物为："
        for item in choking:
            previous_medical_history += item + "，"
        else:
            previous_medical_history = previous_medical_history[:-1] + "。"

    respiratory_infection_history = remove_other(respiratory_infection_history)
    if respiratory_infection_history:
        previous_medical_history += "患"
        for item in respiratory_infection_history:
            previous_medical_history += item + "，"
        else:
            previous_medical_history = previous_medical_history[:-1] + "。"


    medical_history = remove_other(medical_history)
    if medical_history:
        current_medical_history += "服用"
        for item in medical_history:
            current_medical_history += item + "，"

    print("medical_ease",medical_ease)
    medical_ease = remove_other(medical_ease)
    if medical_ease:
        if "有" in medical_ease:
            current_medical_history += "有缓解。"
        elif "无" in medical_ease:
            current_medical_history += "无缓解。"
    else:
        current_medical_history = current_medical_history[:-1]+"。 "

    if cough_name:
        p = "患病名称"+cough_name+"性别"+sex[0]+"年龄"+str(age[0])+"主诉"+complaint+"病症"+current_medical_history+"病史"+previous_medical_history+"过敏史"+allergy_history+"家族史"+famliy_history
        p_dict = {"患病名称":cough_name,"性别":sex[0],"年龄":str(age[0]),"主诉":complaint,"病症":current_medical_history,"病史":previous_medical_history,
                  "过敏史":allergy_history,"家族史":famliy_history}
        return p,p_dict
    else:
        p = "性别" + sex[0] + "年龄" + str(age[0]) + "主诉" + complaint + "病症" + current_medical_history + "病史" + previous_medical_history + "过敏史" + allergy_history + "家族史" + famliy_history
        p_dict = {"性别": sex[0], "年龄": str(age[0]), "主诉": complaint, "病症": current_medical_history,"病史": previous_medical_history,
                  "过敏史": allergy_history, "家族史": famliy_history}
        return p,p_dict


def data_merge_en(s_dict):
    def remove_other(data):
        print("remove_data",data)
        if data:
            if data and ("Other" in data):
                data.remove("Other")
            if data and ("Unclear" in data):
                data.remove("Unclear")
            if data and ("Unused medicine" in data):
                data.remove("Unused medicine")
            return data
        else:
            return data

    # "性别，年龄，主述，病症，病史，过敏史，家族史，患病名称"
    complaint = ""
    current_medical_history = ""
    previous_medical_history = ""
    allergy_history = ""
    famliy_history = ""

    #list
    sex = s_dict.get("性别")
    age = s_dict.get("年龄")
    cough_time = s_dict.get("请问咳嗽出现多久了？")
    sputum = s_dict.get("请问咳嗽是否有痰，或痰极少，不易排出？")
    sputum_color_texture = s_dict.get("请问痰的颜色和质地？")
    cough_start_time = s_dict.get("请问咳嗽发作的时间？")
    past_history = s_dict.get("请问患者过去有哪些病史？")
    allergen = s_dict.get("请问患者过敏源？")
    allerge_famliy = s_dict.get("过敏性疾病家族史？")
    choking = s_dict.get("异物呛咳史？")
    respiratory_infection_history = s_dict.get("有呼吸道感染史？")
    symptom = s_dict.get("请问患者有什么伴随症状？")
    medical_history = s_dict.get("请问患者是否用药治疗？")
    medical_ease = s_dict.get("请问患者用药后是否有所缓解？")
    #str
    cough_name = s_dict.get("患病名称","")

    #主述
    if cough_time:
        complaint += "Already coughed {} {}".format(cough_time[0],cough_time[1].lower())
        if symptom:
            if "Fever" in symptom:
                complaint += ", accompanying fever. "

    #病症
    if sputum_color_texture:
        if sputum_color_texture[1] not in ["Other","Unclear"]:
            current_medical_history += sputum_color_texture[1].lower()
        if sputum_color_texture[0] not in ["Other","Unclear"]:
            current_medical_history += sputum_color_texture[0].lower()
        if current_medical_history:
            current_medical_history += ", "

    if sputum:
        current_medical_history += "Sputum volume: {}. ".format(sputum[0].lower())


    cough_start_time = remove_other(cough_start_time)
    if cough_start_time:
        current_medical_history += "The cough time: "
        for item in cough_start_time:
            current_medical_history += item.lower() + ", "
        else:
            current_medical_history = current_medical_history[:-2] + ". "

    symptom = remove_other(symptom)
    if symptom:
        current_medical_history += "Accompanying symptoms: "
        # current_medical_history += "伴"
        for item in symptom:
            current_medical_history += item.lower() + ", "
        else:
            current_medical_history = current_medical_history[:-2] + ". "

    past_history = remove_other(past_history)
    if past_history:
        for item in past_history:
            previous_medical_history += item.lower() + ", "
        else:
            previous_medical_history = previous_medical_history[:-2] + ". "

    allergen = remove_other(allergen)
    if allergen:
        allergy_history += "The allergen is "
        for item in allergen:
            allergy_history += item.lower() + ", "
        else:
            allergy_history = allergy_history[:-2] + ". "

    allerge_famliy = remove_other(allerge_famliy)
    if allerge_famliy:
        for item in allerge_famliy:
            famliy_history += item.lower() + ", "
        else:
            famliy_history = famliy_history[:-2] + ". "

    choking = remove_other(choking)
    if choking:
        previous_medical_history += "Coughing objects are "
        for item in choking:
            previous_medical_history += item.lower() + ", "
        else:
            previous_medical_history = previous_medical_history[:-2] + ". "

    respiratory_infection_history = remove_other(respiratory_infection_history)
    if respiratory_infection_history:
        previous_medical_history += "Suffer from "
        for item in respiratory_infection_history:
            previous_medical_history += item.lower() + ", "
        else:
            previous_medical_history = previous_medical_history[:-2] + ". "


    medical_history = remove_other(medical_history)
    if medical_history:
        current_medical_history += "Take "
        for item in medical_history:
            current_medical_history += item.lower() + ", "

    print("medical_ease",medical_ease)
    medical_ease = remove_other(medical_ease)
    if medical_ease:
        if "Yes" in medical_ease:
            current_medical_history += "relieved. "
        elif "None" in medical_ease:
            current_medical_history += "not relieved. "
    else:
        current_medical_history = current_medical_history[:-2]+". "

    if cough_name:
        p = "患病名称"+cough_name+"性别"+sex[0]+"年龄"+str(age[0])+"主诉"+complaint+"病症"+current_medical_history+"病史"+previous_medical_history+"过敏史"+allergy_history+"家族史"+famliy_history
        p_dict = {"Cough name":cough_name,"Sex":sex[0],"Age":str(age[0]),"Complaint":complaint,"Current medical history":current_medical_history,
                  "Previous medical history":previous_medical_history,"Allergy history":allergy_history,"Famliy history":famliy_history}
        return p,p_dict
    else:
        p = "性别" + sex[0] + "年龄" + str(age[0]) + "主诉" + complaint + "病症" + current_medical_history + "病史" + previous_medical_history + "过敏史" + allergy_history + "家族史" + famliy_history
        p_dict = {"Sex": sex[0], "Age": str(age[0]), "Complaint": complaint, "Current medical history": current_medical_history,"Previous medical history": previous_medical_history,
                  "Allergy history": allergy_history, "Famliy history": famliy_history}
        return p,p_dict


def data_generator_tfserving(text,structure_fs, tokenizer):
    batch_token_ids, batch_segment_ids,batch_structure_features = [], [], []

    token_ids, segment_ids = tokenizer.encode(text, maxlen=291)
    batch_token_ids.append(token_ids)
    batch_segment_ids.append(segment_ids)
    batch_structure_features.append(structure_fs)
    batch_segment_ids = sequence_padding(batch_segment_ids)
    json_data = json.dumps({"inputs": {"input_data_fl":[structure_fs.tolist()] ,"Input-Token":[token_ids],"Input-Segment":batch_segment_ids.tolist()}})
    return json_data


def structured_feature_processor_fn(feature:list,init_config): #feature : {name:"z"}

    #columns_list = pd.read_csv(r"E:\zhj\most_develop\service\text\cough_data_all_features_validation(1).csv").columns.tolist()
    #columns_dict = {i:0 for i in columns_list}
    with open(init_config, 'r', encoding='utf-8') as f:
        columns_dict = json.loads(f.read())    

    #本地调试
    #for k,v in feature.items():
        #if columns_dict.get(k):
            #columns_dict[k] = 1
            
    for items in feature:
        if columns_dict.get(items['item']):
            columns_dict[items['item']] = 1

    #服务器调试
    # for k,v in feature.items():
    #     feature_name = k+"_"+v
    #     if columns_dict.get(k):
    #         columns_dict[k] = 1
    
    #for items in feature:
        #feature_name = items['item']+"_"+items['belong']
        #if columns_dict.get(feature_name):
            #columns_dict[feature_name] = 1    
    structured_feature = np.array(list(columns_dict.values()))
    return structured_feature

def xstr(s):
    return '' if s is None else str(s)+'。'

def check_processor_fn(feature:dict):
    exam_feature = '检查项目：'+xstr(feature['item'])+'检查描述：'+xstr(feature['describe'])+'检查结果：'+xstr(feature['result'])

    return exam_feature



def data_init(structure_data,exam_data,mr_data,init_config):
    #处理结构化特征
    #with open(init_config, 'r', encoding='utf-8') as f:
        #init_conf_json = json.loads(f.read())
    
    #for items in structure_data:
        #if init_conf_json.get(items['item']):
            #init_conf_json[items['item']] = 1
    
    #structure_feature = np.array(list(init_conf_json.values()))
    structure_feature = structured_feature_processor_fn(structure_data,init_config)
    
    #处理检查+病历 文本类特征
    exam_feature = ''
    for items in exam_data:
        exam_feature += check_processor_fn(items)
    
    text_feature = np.array(list(mr_data + exam_feature))
    
    return [(text_feature,structure_feature)]


def data_generator_tfserving(data, tokenizer):
    #batch_token_ids, batch_segment_ids,batch_structure_features, batch_labels = [], [], [], []
    batch_token_ids, batch_segment_ids,batch_structure_features = [], [], []
    #text,structure_fs, label = data[0]
    text,structure_fs = data[0]
    token_ids, segment_ids = tokenizer.encode(list(text), maxlen=512)
    batch_token_ids.append(token_ids)
    batch_segment_ids.append(segment_ids)
    batch_structure_features.append(structure_fs)
    #batch_labels.append(label)
    batch_token_ids = sequence_padding(batch_token_ids)
    batch_segment_ids = sequence_padding(batch_segment_ids)
    #batch_labels = sequence_padding(batch_labels)  
    json_data = json.dumps({"inputs": {"input_data_fl":[structure_fs.tolist()] ,"Input-Token":[token_ids],"Input-Segment":batch_segment_ids.tolist()}})
    return json_data


def prediction_lookup(sorted_list,language):
    if language=="en":
        init_config = '/home/slave1/test_zhj/service/conf/label_name_en.json'
    else:
        init_config='/home/slave1/test_zhj/service/conf/label_name.json'
    with open(init_config, 'r', encoding='utf-8') as f:
        label_dict = json.loads(f.read())  

    prediction = [label_dict.get(str(item)) for item in sorted_list]
    
    return prediction
