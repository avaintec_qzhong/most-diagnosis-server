#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
os.environ['TF_KERAS'] = '1'
from data_assembly import pre_interview_param_get,inception_param_get,assembly
import cdss_processor
from bert4keras.tokenizers import Tokenizer
import requests
import json
import numpy as np
import pandas as pd
from flask import Flask, request, Response
from flask_cors import CORS
app = Flask(__name__)
CORS(app, resources=r'/*')

inception_label_list = np.array(['血常规', '异淋', '超敏CRP测定', '过敏源测定', '免疫球蛋白', '补体', '胸部正位', '门诊四联呼吸道病毒快速检测', '支气管舒张试验', '流速容量曲线'])
inception_label_en_list = np.array(['Blood routine', 'Different shower', 'Ultra-sensitive CRP determination', 'Allergen determination',
                                    'Immunoglobulin', 'Complement', 'Chest upright', 'Rapid respiratory virus detection', 'Bronchodilation test',
                                    'Flow capacity curve'])
#院外
pre_interview_label_list = np.array(['J20', 'J06', 'J18', 'J45', 'J11', 'J40', 'J00', 'R50', 'R05', 'J02', 'J21', 'J30'])
pre_interview_label_en_list = np.array(['Acute bronchitis', 'Acute upper respiratory tract infection', 'Acute bronchopneumonia',
                                        'Asthmatic bronchitis', 'Influenza', 'Bronchitis', 'Acute nasopharyngitis',
                                        'Fever', 'Chronic cough', 'Acute pharyngitis', 'Acute asthmatic bronchitis', 'Allergic rhinitis'])
#院内
# pre_interview_label_list = np.array(['急性上呼吸道感染','支气管炎', '哮喘', '咽炎', '肺炎', '鼻炎', '扁桃体炎', '喉炎', '鼻窦炎', '流行性感冒',
#                                           '气道异物', '疑难杂症'])
# s = "年龄8岁性别女主诉:复诊发热咳嗽（4天余）病症:患儿仍有发热，最高体温39℃，有咳嗽，阵发性连声，有喉头痰鸣，无呕吐腹泻，胃纳一般。给予奥司他韦口服4天。否认结核接触史及异物呛咳史病史:无过敏史，出生抢救史，重大疾病史，患病名称:流行性感冒体格检查:神清，精神可，咽部充血，双肺未闻及明显干湿罗音，心音中，律齐，腹部平软"
inception_param  = inception_param_get()
pre_interview_param  = pre_interview_param_get()

#bert
#dict_path=r'E:\zhj\most_develop\service\data\bert_vocab.txt'
dict_path=r'/home/slave1/test_zhj/service/data/bert_vocab.txt'
tokenizer = Tokenizer(dict_path, do_lower_case=True)

@app.route('/pre_interview/', methods=['POST'])
def pre_interview_predict():
    s_dict = request.get_json().get("s")
    language = request.get_json().get("locale")
    # language = request.get_json().get("s").get("locale")

    print("language",language)
    print(s_dict)
    if language=="en":
        p, p_dict = cdss_processor.data_merge_en(s_dict)
    else:
        p,p_dict = cdss_processor.data_merge(s_dict)
    print(p)
    if p:
        s_ids = assembly(p,pre_interview_param[0],pre_interview_param[1],pre_interview_param[2])
        data = json.dumps({"inputs": {"input_data":s_ids}})
        headers = {"content-type": "application/json"}
        json_response = requests.post('http://172.16.70.157:8501/v1/models/pre_interview_model:predict',data=data, headers=headers)
        predictions = json.loads(json_response.text)
        output = np.array(predictions.get("outputs"))
        print(output)
        sort_index = np.argsort(output[0])[::-1]
        if language == "en":
            res = {"data": list(pre_interview_label_en_list[sort_index]), "prob": list(output[0][sort_index]),
                   "p_dict": p_dict}
            ####inception
            top_3_name = pre_interview_label_en_list[sort_index[:3]]
        else:
            res = {"data": list(pre_interview_label_list[sort_index]), "prob": list(output[0][sort_index]),
                   "p_dict": p_dict}
            ####inception
            top_3_name = pre_interview_label_list[sort_index[:3]]
        inception_res_list = []
        for n, item in enumerate(top_3_name):
            inception_p = "患病名称{}".format(item) + p
            inception_res = inception_fn(inception_p,language)
            inception_res["患病名称_{}".format(n)] = item
            inception_res_list.append(inception_res)
        res["inception_res_top3"] = json.dumps(inception_res_list,ensure_ascii=False)
        print(inception_res_list)

        return Response(json.dumps(res), mimetype='application/json')
    else:
        res = {"data": None,"prob":None,"p_dict":None,"inception_res_top3":None}
        # return jsonify(res)
        return Response(json.dumps(res), mimetype='application/json')



# @app.route('/inception/', methods=['POST'])
# def inception_predict():
#     s_dict = request.get_json().get("s")
#     print(s_dict)
#     p,p_dict = cdss_processor.data_merge(s_dict)
#     print(p)
#     if p:
#         s_ids = assembly(p,inception_param[0],inception_param[1],inception_param[2])
#         data = json.dumps({"inputs": {"input_data":s_ids}})
#         headers = {"content-type": "application/json"}
#         json_response = requests.post('http://172.16.53.50:8501/v1/models/inception_model:predict',data=data, headers=headers)
#         predictions = json.loads(json_response.text)
#         output = np.array(predictions.get("outputs"))
#         sort_index = np.argsort(output[0])[::-1]
#         print("sort_index",sort_index)
#         res1 = {"data": list(inception_label_list[sort_index]), "prob": list(output[0][sort_index])}
#
#         mask = np.array(output[0])>= 0.5
#         label = list(inception_label_list[mask])
#         res2 = {"predict":label}
#         res3 = {"p_dict":p_dict}
#         # return jsonify(dict(dict(res2,**res1),**res3))
#         return Response(json.dumps(dict(dict(res2,**res1),**res3)), mimetype='application/json')
#     else:
#         res = {"data": None,"prob":None,"predict":None,"p_dict":None}
#         # return jsonify(res)
#         return Response(json.dumps(res), mimetype='application/json')

def inception_fn(p,language):
    if p:
        s_ids = assembly(p,inception_param[0],inception_param[1],inception_param[2])
        data = json.dumps({"inputs": {"input_data":s_ids}})
        headers = {"content-type": "application/json"}
        json_response = requests.post('http://172.16.70.157:8501/v1/models/inception_model:predict',data=data, headers=headers)
        predictions = json.loads(json_response.text)
        output = np.array(predictions.get("outputs"))
        sort_index = np.argsort(output[0])[::-1]
        print("sort_index",sort_index)
        res1 = {"data": list(inception_label_list[sort_index]), "prob": list(output[0][sort_index])}
        mask = np.array(output[0])>= 0.5
        if language == "en":
            label = list(inception_label_en_list[mask])
        else:
            label = list(inception_label_list[mask])
        res2 = {"predict":label}
        # return jsonify(dict(dict(res2,**res1),**res3))
        return dict(res2,**res1)
    else:
        res = {"data": None,"prob":None,"predict":None}
        # return jsonify(res)
        return res


@app.route('/cdss/', methods=['POST'])
def cdss_predict():
    init_config = r'/home/slave1/test_zhj/service/conf/init_config.json'
    #init
    s_dict = request.get_json().get("s")
    language = request.get_json().get("locale")
    print("s_dict",s_dict)
    print("language",language)
    if language == "en":
        if s_dict:
            p = "Sex" + s_dict.get("Sex") + "Age" + str(s_dict.get("Age")) + "Complaint" + s_dict.get("Complaint") + \
                "Current medical history" + s_dict.get("Current medical history") + "Previous medical history" + s_dict.get("Previous medical history") \
                + "Allergy history" + s_dict.get("Allergy history") + "Famliy history" + s_dict.get("Famliy history")
        else:
            p = ""
    else:
        if s_dict:
            p = "性别" + s_dict.get("性别") + "年龄" + str(s_dict.get("年龄")) + "主诉" + s_dict.get("主诉") + \
                "病症" + s_dict.get("病症") + "病史" + s_dict.get("病史") + "过敏史" + s_dict.get("过敏史") + "家族史" + s_dict.get("家族史")
        else:
            p = ""

    #结构化数据
    structured_feature = request.get_json().get("i")
    check_feature = request.get_json().get("c")
    print("structured_feature",structured_feature)
    print("check_feature",check_feature)
    #res = {"data": ["哮喘","肺炎","支气管炎"], "prob": [0.3,0.2,0.1], "predict": ["哮喘","肺炎"]}
    #return Response(json.dumps(res), mimetype='application/json')
    

    #structured_feature = cdss_processor.structured_feature_processor_fn(structured_feature)
    #check_feature = cdss_processor.check_processor_fn(check_feature)
    valid_data = cdss_processor.data_init(structured_feature, check_feature, p, init_config)
    #assert len(structured_feature) == 423

    #自然语言
    # p,p_dict = cdss_processor.data_merge(s_dict)
    if p:
        #textcnn 自然语言处理方式
        # s_ids = assembly(p,inception_param[0],inception_param[1],inception_param[2])
        # data = json.dumps({"inputs": {"input_data":s_ids}})
        # headers = {"content-type": "application/json"}
        # json_response = requests.post('http://172.16.53.50:8501/v1/models/inception_model:predict',data=data, headers=headers)
        #mcbert处理方式

        #json_data = cdss_processor.data_generator_tfserving(p,structured_feature,tokenizer)
        json_data = cdss_processor.data_generator_tfserving(valid_data,tokenizer)
        headers = {"content-type": "application/json"}
        json_response = requests.post('http://172.16.70.157:8501/v1/models/multimodal:predict', data=json_data,headers=headers)
        predictions = json.loads(json_response.text)
        output = np.array(predictions.get("outputs"))
        sort_index = list(np.argsort(output[0])[::-1])
        print("sort_index",sort_index)
        
        sorted_predict_prob = [output[0][index] for index in sort_index]
        sorted_predict_data = cdss_processor.prediction_lookup(sort_index,language)
        
        res = {"data": sorted_predict_data[:3], "prob": sorted_predict_prob[:3], "predict": sorted_predict_data[:3]}
        return Response(json.dumps(res), mimetype='application/json')
    
    else:
        res = {"data": None,"prob":None,"predict":None,"p_dict":None}
        # return jsonify(res)
        return Response(json.dumps(res), mimetype='application/json')


if __name__ == '__main__':
    app.run(host="0.0.0.0",port="8081",debug=True)



