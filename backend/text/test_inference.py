#!/usr/bin/python
# -*- coding: UTF-8 -*-
from physical_library_layer.dnn_model_builder_pl import pl_DNN
from medical_record_layer.mc_bert_using_b4k import mc_bert
from fusion_layer.mutan_keras import mutan_fusion
from bert4keras.tokenizers import Tokenizer
import yaml
import utils
import numpy as np
from text import data_processor
from bert4keras.snippets import sequence_padding
import tensorflow as tf
import json

#class data_generator(DataGenerator):
    #"""数据生成器
    #"""
    #def __iter__(self, random=False):
        #batch_token_ids, batch_segment_ids,batch_structure_features, batch_labels = [], [], [], []
        #for is_end, (text, structure_fs, label) in self.sample(random):
            #token_ids, segment_ids = tokenizer.encode(text, maxlen=291)
            #batch_token_ids.append(token_ids)
            #batch_segment_ids.append(segment_ids)
            #batch_structure_features.append(structure_fs)
            #batch_labels.append(label)
            #if len(batch_token_ids) == self.batch_size or is_end:
                #batch_token_ids = sequence_padding(batch_token_ids)
                #batch_segment_ids = sequence_padding(batch_segment_ids)
                #batch_labels = sequence_padding(batch_labels)
                #return np.array(batch_token_ids), np.array(batch_segment_ids), np.array(batch_structure_features)

def data_generator(data, tokenizer):
    batch_token_ids, batch_segment_ids,batch_structure_features, batch_labels = [], [], [], []
    text,structure_fs, label = data[0]
    token_ids, segment_ids = tokenizer.encode(text, maxlen=291)
    batch_token_ids.append(token_ids)
    batch_segment_ids.append(segment_ids)
    batch_structure_features.append(structure_fs)
    batch_labels.append(label)
    batch_token_ids = sequence_padding(batch_token_ids)
    batch_segment_ids = sequence_padding(batch_segment_ids)
    batch_labels = sequence_padding(batch_labels)
    return np.array(batch_token_ids), np.array(batch_segment_ids), np.array(batch_structure_features)    


def data_generator_tfserving(data, tokenizer):
    batch_token_ids, batch_segment_ids,batch_structure_features, batch_labels = [], [], [], []
    text,structure_fs, label = data[0]
    token_ids, segment_ids = tokenizer.encode(text, maxlen=291)
    batch_token_ids.append(token_ids)
    batch_segment_ids.append(segment_ids)
    batch_structure_features.append(structure_fs)
    batch_labels.append(label)
    batch_token_ids = sequence_padding(batch_token_ids)
    batch_segment_ids = sequence_padding(batch_segment_ids)
    batch_labels = sequence_padding(batch_labels)
    json_data = json.dumps({"inputs": {"input_data_fl":[structure_fs.tolist()] ,"Input-Token":[token_ids],"Input-Segment":batch_segment_ids.tolist()}})
    return json_data



options = {}
with open('conf/mutan_noatt_train.yaml', 'r') as conf:
    options_yaml = yaml.load(conf)
options = utils.update_values(options, options_yaml)       

structure_model = pl_DNN(423, 0.5,27)
#用textcnn
#text_model = TextCNN(vocab_size, args.text_length, args.embedding_dims, args.num_classes, args.num_filters, args.filter_sizes, args.regularizers_lambda, args.dropout_rate, embedding_matrix)

config_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_config.json'
checkpoint_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_model.ckpt' 
dict_path='D:\\python_workspace\\BERT\\mc_bert\\bert_vocab.txt'

tokenizer = Tokenizer(dict_path, do_lower_case=True)

text_model = mc_bert(config_path, checkpoint_path)

valid_data = data_processor.data_converge('data/cough_data_all_features_validation.csv', 291, 27)
valid_generator = data_generator(valid_data, tokenizer)  

valid_generator_json = data_generator_tfserving(valid_data, tokenizer)

model = mutan_fusion(text_model, structure_model, 27, options['model']['fusion'], structure_embedding = True, text_embedding = True)    
#train(save_path,vocab_size,embedding_matrix,'concatenation')
model.load_weights('model/multimodal_mcbert.h5')

pb_model = tf.saved_model.load('model/multimodal')
res = model.predict(valid_generator)
print()
    
